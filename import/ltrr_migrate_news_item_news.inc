<?php
/**
 * Makes UAQS News content from the old LTRR News Item nodes (the main node content).
 */
class LtrrMigrateNewsItemNewsMigration extends DrupalNode7Migration {

  /**
   * Constructor.
   *
   * @param array $arguments
   *   Arguments for the parent constructor (Migration standard behavior).
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // Documented lists of source data fields:.
    $fields = array(
      'body' => t('Body'),
      'field_image_caption' => t('Caption'),
      'field_image_credit' => t('Credit'),
      'field_news_image' => t('News Image'),
      'field_news_priority' => t('Priority'),
      'paragraph_items' => t('Link paragraphs'),
    );

    $image_src_field = 'field_news_image';

    // The straight one-to-one mappings.
    $this->addSimpleMappings(array('title'));

    // Direct field mappings.
    $this->addFieldMapping('field_uaqs_body', 'body');
    $this->addFieldMapping('field_uaqs_summary', 'body:summary');
    $this->addFieldMapping('field_uaqs_short_title', 'title');
    $this->addFieldMapping('field_uaqs_published', 'created');
    $this->addFieldMapping('expiration_date')
         ->defaultValue('2026-08-22 11:30:00');

    // The link field is a special case.
    $this->addFieldMapping('field_uaqs_link')
         ->defaultValue('[node:url]');
    $this->addFieldMapping('field_uaqs_link:title')
         ->defaultValue('Read more');

    // Image fields are more complicated.
    $image_dst_field = 'field_uaqs_photo';
    $this->addFieldMapping($image_dst_field, $image_src_field);
    $this->addFieldMapping($image_dst_field . ':file_class')
         ->defaultValue('MigrateFileFid');
    $this->addFieldMapping($image_dst_field . ':preserve_files')
         ->defaultValue(TRUE);
    $this->addFieldMapping($image_dst_field . ':alt', $image_src_field . ':alt');
    $this->addFieldMapping($image_dst_field . ':title', $image_src_field . ':title');

    // Paragraphs (UAQS Content Chunks).
    $this->addFieldMapping('field_uaqs_main_content', 'paragraph_items');

    // Allow limited HTML markup in the body field.
    $this->addFieldMapping('field_uaqs_body:format')
         ->defaultValue('uaqs_unconstrained');
  }

  /**
   * Populate the row with the source bundle field data.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Paragraph items by direct database lookup.
    $pquery = db_select('migrate_map_ltrrnewsitemprimaryextra', 'p')
              ->condition('p.sourceid1', $row->nid)
              ->fields('p', array('destid1'));
    $primary = $pquery->execute()->fetchCol(0);
    $squery = db_select('migrate_map_ltrrnewsitemsecondaryextra', 's')
              ->condition('s.sourceid1', $row->nid)
              ->fields('s', array('destid1'));
    $secondary = $squery->execute()->fetchCol(0);
    $row->paragraph_items = array_merge($primary, $secondary);
  }

}

/**
 * Defines migration from Node fields into Paragraphs.
 *
 * The migrate_d2d module already provides the class that sets up an existing
 * Drupal database as the migration source, but the DrupalNodeMigration class
 * there assumes a node destination as well, so we can't sub-class it.
 * The Migration module has no direct support for Paragraphs Items
 * (which are use freestanding entities, distinct from nodes), but the
 * Paragraphs module itself defines a Migration api.
 */
abstract class LtrrNodeParagraphsMigration extends DrupalMigration {

  /**
   * Constructor.
   *
   * @param array $arguments
   *   Arguments for the parent constructor (Migration standard behavior).
   * @param string $bundle_dest
   *   The Paragraph type machine name.
   * @param string $bundle_src
   *   The Node type machine name.
   * @param string $field_name
   *   Name of the field where the Paragraphs attach to a host entity.
   * @param string $description
   *   Detailed information describing the migration.
   */
  public function __construct(array $arguments, $bundle_dest, $bundle_src, $field_name, $description) {
    $arguments['source_version'] = 7; // Force Drupal 7: no support for 6 here.
    $arguments['source_type'] = $bundle_src; // Override a pre-specified bundle.
    parent::__construct($arguments);

    $this->description = $description;

    $this->sourceFields += $this->version->getSourceFields('node', $bundle_src);

    $this->source = new MigrateSourceSQL($this->query(), $this->sourceFields, NULL,
      $this->sourceOptions);

    $this->destination = new MigrateDestinationParagraphsItem(
      $bundle_dest,
      array(
        'field_name' => $field_name,
        'text_format' => 'uaqs_textual_content',
      )
    );

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'nid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'Source node ID',
          'alias' => 'n',
        ),
      ),
      MigrateDestinationParagraphsItem::getKeySchema()
    );

    if (!$this->newOnly) {
      $this->highwaterField = array(
        'name' => 'changed',
        'alias' => 'n',
        'type' => 'int',
      );
    }
  }

  /**
   * Query for basic node fields from Drupal 7.
   * The migrate_d2d module defines this for Drupal 7 node migrations, but we
   * don't have access to that implementation (not being a subclass of its
   * DrupalNode7Migration).
   *
   * @return QueryConditionInterface
   */
  protected function query() {
    $query = Database::getConnection('default', $this->sourceConnection)
             ->select('node', 'n')
             ->fields('n', array('nid', 'vid', 'language', 'title', 'uid',
               'status', 'created', 'changed', 'comment', 'promote', 'sticky',
               'tnid', 'translate'))
             ->condition('n.type', $this->sourceType)
             ->orderBy($this->newOnly ? 'n.nid' : 'n.changed');
    // Join node_counter for Statistics support
    if (Database::getConnection('default', $this->sourceConnection)
        ->schema()->tableExists('node_counter')) {
      $query->leftJoin('node_counter', 'nc', 'n.nid=nc.nid');
      $query->addField('nc', 'daycount');
      $query->addField('nc', 'timestamp');
      $query->addField('nc', 'totalcount');
    }
    return $query;
  }

  /**
   * Populate the row with the source bundle field data.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $this->version->getSourceValues($row, $row->nid);
  }

}

/**
 * Defines migration of News Info primary link fields into Paragraphs.
 */
class LtrrNewsItemPrimaryExtraMigration extends LtrrNodeParagraphsMigration {

  /**
   * Constructor.
   *
   * @param array $arguments
   *   Arguments for the parent constructor (Migration standard behavior).
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments, 'uaqs_extra_info', 'news_item', 'field_uaqs_main_content',
      t('News Item Primary Link fields to UAQS Extra Info entities.'));

    // Fields to import to the destination bundle.
    $fields = array(
      'field_news_link1' => t('More information links'),
      'field_news_link1:title' => t('More information link title'),
    );

    // Map node fields to paragraphs fields and subfields.
    $this->addFieldMapping('field_uaqs_link', 'field_news_link1');
    $this->addFieldMapping('field_uaqs_link:title', 'field_news_link1:title');

  }

}

/**
 * Defines migration of News Info secondary link fields into Paragraphs.
 */
class LtrrNewsItemSecondaryExtraMigration extends LtrrNodeParagraphsMigration {

  /**
   * Constructor.
   *
   * @param array $arguments
   *   Arguments for the parent constructor (Migration standard behavior).
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments, 'uaqs_extra_info', 'news_item', 'field_uaqs_main_content',
      t('News Item Secondary Link fields to UAQS Extra Info entities.'));

    // Fields to import to the destination bundle.
    $fields = array(
      'field_news_link2' => t('More information links'),
      'field_news_link2:title' => t('More information link title'),
    );

    // Map node fields to paragraphs fields and subfields.
    $this->addFieldMapping('field_uaqs_link', 'field_news_link2');
    $this->addFieldMapping('field_uaqs_link:title', 'field_news_link2:title');

  }

}

